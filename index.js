import express, { json } from "express"
import { firstRouter } from "./src/route/firstRouter.js"
import { schoolsRouter } from "./src/route/schoolsRouter.js"
import { vehiclesRouter } from "./src/route/vehiclesRouter.js"
import { connectToMongodb } from "./src/connectToDb/connectToMongodb.js"
import { studentRouter } from "./src/route/studentRouter.js"
import { classRoomRouter } from "./src/route/classRoomRouter.js"
import { departMentRouter } from "./src/route/departMentRouter.js"
import { teacherRouter } from "./src/route/teacherRouter.js"
import { bookRouter } from "./src/route/bookRouter.js"
import { contactRouter } from "./src/route/ContactRouter.js"
import { productRouter } from "./src/route/productRouter.js"
import { userRouter } from "./src/route/userRouter.js"
import { reviewRouter } from "./src/route/reviewRouter.js"
import { traineeRouter } from "./src/route/traineeRouter.js"
import { collegeRouter } from "./src/route/collegeRouter.js"
import { webUserRouter } from "./src/route/webUserRouter.js"



let expressApp = express()
expressApp.use(json())
connectToMongodb()
//Global middleware
// expressApp.use((req,res,next)=>{
//     console.log("I am application middlewareq")
//     next()
// })

expressApp.use("/bike", firstRouter)
expressApp.use("/trainees", traineeRouter)
expressApp.use("/colleges", collegeRouter)
expressApp.use("/schools", schoolsRouter)
expressApp.use("/vehicles", vehiclesRouter)
expressApp.use("/students", studentRouter)
expressApp.use("/teachers", teacherRouter)
expressApp.use("/books", bookRouter)
expressApp.use("/classrooms", classRoomRouter)
expressApp.use("/departments", departMentRouter)
expressApp.use("/contacts", contactRouter)
expressApp.use("/products", productRouter)
expressApp.use("/users", userRouter)
expressApp.use("/reviews", reviewRouter)
expressApp.use("/web-users", webUserRouter)

import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { port, secretKey } from "./src/constant.js"


// console.log(process.env.NAME)

expressApp.listen(port,()=>{
    console.log("app is listening at port 8000")
})

// let infoObject = {
//     name : "Anuj",
//     age : 40,
// }

// let expiryInfo = {
//     expiresIn : "365d",
 
// }

// let token = jwt.sign(infoObject, secretKey, expiryInfo)

// console.log(token)

//verify token
// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoibml0YW4iLCJhZ2UiOjI5LCJpYXQiOjE3MDY0MjI0NzAsImV4cCI6MTczNzk1ODQ3MH0.5-lQr3tv2H0HwLJ6RKw7p6_U1nd7P3vkhVJ6X-k7m-w"

// try {
//     let inObj = jwt.verify(token, "dw11")
//     console.log(inObj)
// } catch (error) {
//     console.log(error.message)
    
// } 



/* 
console.log("My name is Anuj Shrestha")

make backend application (using express)
attached port to that application */

//password hashing (needs to be importing import bcrypt from "bcrypt" and install npm i bcrypt)
// let password = "abc@123"
// let hashpassword = await bcrypt.hash(password, 10)
// console.log(hashpassword)

//compare hash password
// let hashPassword = "$2b$10$IFcM91cAL1RmVBe28mGmOeuLGJh3Y/TW7.MArKSpvbxnS4T8hnmdy" //hashed output of "namaste123"
// let password = "namaste123"
// let isPasswordMatch = await bcrypt.compare(password, hashPassword)
// console.log(isPasswordMatch) 
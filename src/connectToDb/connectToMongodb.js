import mongoose from "mongoose";
import { mongoUrl } from "../constant.js"


export let connectToMongodb = () => {
  mongoose.connect(mongoUrl);
};

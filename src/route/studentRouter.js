import { Router } from "express";
import { Student } from "../schema/model.js";
import { createStudent, deleteStudent, getAllStudent, getSpecificStudent, updateStudent } from "../controller/studentController.js";
export let studentRouter = Router()

studentRouter
.route("/")
.post(createStudent)
.get(getAllStudent)

studentRouter
.route("/:id")
.get(getSpecificStudent)
.patch(updateStudent)
.delete(deleteStudent)
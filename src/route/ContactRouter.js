import { Router } from "express";
import { createContact, deleteContact, getAllContact, getSpecificContact, updateContact } from "../controller/contactController.js";
export let contactRouter = Router()

contactRouter
.route("/")
.post(createContact)
.get(getAllContact)

contactRouter
.route("/:id")
.get(getSpecificContact)
.patch(updateContact)
.delete(deleteContact)
import { Router } from "express";
import { createProduct, deleteProduct, getAllProduct, getSpecificProduct, updateProduct } from "../controller/productController.js";
export let productRouter = Router()

productRouter
.route("/")
.post(createProduct)
.get(getAllProduct)

productRouter
.route("/:id")
.get(getSpecificProduct)
.patch(updateProduct)
.delete(deleteProduct)
import { Router } from "express";
import { createClassRoom, 
    deleteClassRoom, 
    getAllClassRoom, 
    getSpecificClassRoom, 
    updateClassRoom 
} from "../controller/classRoomController.js";
export let classRoomRouter = Router()

classRoomRouter
.route("/")
.post(createClassRoom)
.get(getAllClassRoom)

classRoomRouter
.route("/:id")
.get(getSpecificClassRoom)
.patch(updateClassRoom)
.delete(deleteClassRoom)
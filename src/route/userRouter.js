import { Router } from "express";
import { createUser, deleteUser, getAllUser, getSpecificUser, updateUser } from "../controller/userController.js";
export let userRouter = Router()

userRouter
.route("/")
.post(createUser)
.get(getAllUser)

userRouter
.route("/:id")
.get(getSpecificUser)
.patch(updateUser)
.delete(deleteUser)
import { Router } from "express";
import { Book } from "../schema/model.js";
import { createBook, deleteBook, getAllBook, getSpecificBook, updateBook } from "../controller/bookController.js";
export let bookRouter = Router()

bookRouter
.route("/")
.post(createBook)
.get(getAllBook)

bookRouter
.route("/:id")
.get(getSpecificBook)
.patch(updateBook)
.delete(deleteBook)
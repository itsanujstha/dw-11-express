import { Router } from "express";
import { Teacher } from "../schema/model.js";
import { createTeacher, deleteTeacher, getAllTeacher, getSpecificTeacher, updateTeacher } from "../controller/teacherController.js";
export let teacherRouter = Router()

teacherRouter
.route("/")
.post(createTeacher)
.get(getAllTeacher)

teacherRouter
.route("/:id")
.get(getSpecificTeacher)
.patch(updateTeacher)
.delete(deleteTeacher)
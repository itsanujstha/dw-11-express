import { Router } from "express";
import { createReview, deleteReview, getAllReview, getSpecificReview, updateReview } from "../controller/reviewController.js";
export let reviewRouter = Router()

reviewRouter
.route("/")
.post(createReview)
.get(getAllReview)

reviewRouter
.route("/:id")
.get(getSpecificReview)
.patch(updateReview)
.delete(deleteReview)
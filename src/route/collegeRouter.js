import { Router } from "express";
import { createCollege, deleteCollege, getAllCollege, getSpecificCollege, updateCollege } from "../controller/collegeController.js";
export let collegeRouter = Router()

collegeRouter
.route("/")
.post(createCollege)
.get(getAllCollege)

collegeRouter
.route("/:id")
.get(getSpecificCollege)
.patch(updateCollege)
.delete(deleteCollege)
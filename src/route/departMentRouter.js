import { Router } from "express";
import { createDepartMent, deleteDepartMent, getAllDepartMent, getSpecificDepartMent, updateDepartMent } from "../controller/departMentController.js";
export let departMentRouter = Router()

departMentRouter
.route("/")
.post(createDepartMent)
.get(getAllDepartMent)

departMentRouter
.route("/:id")
.get(getSpecificDepartMent)
.patch(updateDepartMent)
.delete(deleteDepartMent)
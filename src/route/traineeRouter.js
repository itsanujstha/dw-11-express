import { Router } from "express";
import { createTrainee, deleteTrainee, getAllTrainee, getSpecificTrainee, updateTrainee } from "../controller/traineeController.js";
export let traineeRouter = Router()

traineeRouter
.route("/")
.post(createTrainee)
.get(getAllTrainee)

traineeRouter
.route("/:id")
.get(getSpecificTrainee)
.patch(updateTrainee)
.delete(deleteTrainee)
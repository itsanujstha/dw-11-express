import { Router } from "express";
export let vehiclesRouter = Router()

vehiclesRouter
.route("/")
.post((req,res,next)=>{
    let data = req.body

    res.json(data)
})
.get((req,res,next)=>{
    res.json({
        success : true,
        message : "Vehicles read successfully."
    })
})
.patch((req,res,next)=>{
    res.json({
        success : true,
        message : "Vehicles updated successfully."
    })
})
.delete((req,res,next)=>{
    res.json({
        success : true,
        message : "Vehicles deleted successfully."
    })
})

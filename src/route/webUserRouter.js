import { Router } from "express";
import { createWebUser, deleteWebUser, getAllWebUser, getSpecificWebUser, loginUser, myProfile, updateMyProfile, updateWebUser, verifyEmail } from "../controller/webUserController.js";
import { secretKey } from "../constant.js";
import jwt from "jsonwebtoken"
import isAuthenticated from "../middleware/isAuthenticated.js";
export let webUserRouter = Router()

webUserRouter
.route("/")
.post(createWebUser)
webUserRouter.route("/verify-email").patch(verifyEmail)
webUserRouter.route("/login").post(loginUser)
webUserRouter.route("/my-profile").get(isAuthenticated, myProfile)
webUserRouter.route("/update-myProfile").patch(isAuthenticated, updateMyProfile)


webUserRouter.route("/admin").get(getAllWebUser).get(getSpecificWebUser).patch(updateWebUser).delete(deleteWebUser)





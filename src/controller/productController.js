import { Product } from "../schema/model.js"

export let createProduct = async(req,res,next)=>{
    let data = req.body
    //console.log("in post")
    try {
        let result = await Product.create(data)
        res.json({

            success: true,
            message: "student product created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllProduct = async (req,res,next)=>{
    try {
    // let result = await Product.find({})
    
    //pagination
    //localhost:8000/products?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    let result = await Product.find({}).skip((page-1)*limit).limit(limit)
        res.json({

            success: true,
            message: "All product read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all product successfully.",
        })
    }
    
    

    
}

export let getSpecificProduct = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await Product.findById(id)
        
        res.json({

            success: true,
            message: "product read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateProduct = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Product.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "product updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteProduct = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Product.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "product deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
import { User } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendmail.js"

export let createUser = async(req,res,next)=>{
    let data = req.body
    let password = data.password
    let hashPassword = await bcrypt.hash(password, 10)
    data.password = hashPassword

    try {
        let result = await User.create(data)
        sendEmail({
            from: "Uniq systems <burningonfire@gmail.com>",
            to: [data.email],
            subject: "Register successfully.",
            html:`
            <div>
            <p> you have sucessfully registered in our system.</p>
            `,
        })

        res.json({

            success: true,
            message: "user created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllUser = async (req,res,next)=>{
    try {
    // let result = await User.find({})
    
    //pagination
    //localhost:8000/users?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    let result = await User.find({}).skip((page-1)*limit).limit(limit)
        res.json({

            success: true,
            message: "All user read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all user successfully.",
        })
    }
    
    

    
}

export let getSpecificUser = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await User.findById(id)
        
        res.json({

            success: true,
            message: "user read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateUser = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await User.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "user updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteUser = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await User.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "user deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
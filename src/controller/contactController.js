import { Contact } from "../schema/model.js"

export let createContact = async(req,res,next)=>{
    let data = req.body
    console.log("in post")
    try {
        let result = await Contact.create(data)
        res.json({

            success: true,
            message: "student contact created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllContact = async (req,res,next)=>{
    try {
    // let result = await Contact.find({})
    
    //pagination
    //localhost:8000/contacts?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    let result = await Contact.find({}).skip((page-1)*limit).limit(limit)
        res.json({

            success: true,
            message: "All student contact read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all student contact successfully.",
        })
    }
    
    

    
}

export let getSpecificContact = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await Contact.findById(id)
        
        res.json({

            success: true,
            message: "student contact read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateContact = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Contact.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "student contact updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteContact = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Contact.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "student contact deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
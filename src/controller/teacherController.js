import { Teacher } from "../schema/model.js"


export let createTeacher = async(req,res,next)=>{
    let data = req.body
    
    try {
        let result = await Teacher.create(data)

        res.json({

            success: true,
            message: "teacher created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllTeacher = async (req,res,next)=>{
    try {
        let result = await Teacher.find({})
        

        res.json({
        
            success: true,
            message: "teacher read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read teacher successfully.",
        })
    }
    
    

    
}

export let getSpecificTeacher = async(req,res,next)=>{
    let id = req.params.id
    console.log(id)

    try {
        let result = await Teacher.findById(id)
        
        res.json({

            success: true,
            message: "teacher read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateTeacher = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Teacher.findByIdAndUpdate(id, data, {new: true}) 
    //if {new:false} will throw old data
    //if{new: true} will throw new data

    res.json({
        success : true,
        message: "teacher updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteTeacher = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Teacher.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "teacher deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
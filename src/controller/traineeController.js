import { Trainee } from "../schema/model.js"

export let createTrainee = async(req,res,next)=>{
    let data = req.body
    console.log("in post")
    try {
        let result = await Trainee.create(data)
        res.json({

            success: true,
            message: "trainee created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllTrainee = async (req,res,next)=>{
    try {
    // let result = await Trainee.find({})
    
    //pagination
    //localhost:8000/trainees?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    let result = await Trainee.find({}).skip((page-1)*limit).limit(limit)
        res.json({

            success: true,
            message: "All trainee read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all trainee successfully.",
        })
    }
    
    

    
}

export let getSpecificTrainee = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await Trainee.findById(id)
        
        res.json({

            success: true,
            message: "trainee read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateTrainee = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Trainee.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "trainee updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteTrainee = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Trainee.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "trainee deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
import { ClassRoom } from "../schema/model.js"

export let createClassRoom
 = async(req,res,next)=>{
    let data = req.body
    try {
        let result = await ClassRoom.create(data)
        res.json({

            success: true,
            message: "Classroom created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            message: error.message
        })
    }
}

export let getAllClassRoom = async (req,res,next)=>{
    try {
        let result = await ClassRoom.find({})

        res.json({

            success: true,
            message: "All classrooms read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all classrooms successfully.",
        })
    }
    
    

    
}

export let getSpecificClassRoom = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await ClassRoom.findById(id)
        
        res.json({

            success: true,
            message: "ClassRoom read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateClassRoom = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await ClassRoom.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "Classroom updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteClassRoom = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await ClassRoom.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "ClassRoom deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
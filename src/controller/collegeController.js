import { College } from "../schema/model.js"

export let createCollege = async(req,res,next)=>{
    let data = req.body
    console.log("in post")
    try {
        let result = await College.create(data)
        res.json({

            success: true,
            message: "college created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllCollege = async (req,res,next)=>{
    try {
    // let result = await College.find({})
    
    //pagination
    //localhost:8000/colleges?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    let result = await College.find({}).skip((page-1)*limit).limit(limit)
        res.json({

            success: true,
            message: "All college read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all college successfully.",
        })
    }
    
    

    
}

export let getSpecificCollege = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await College.findById(id)
        
        res.json({

            success: true,
            message: "college read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateCollege = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await College.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "college updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteCollege = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await College.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "college deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
import { WebUser } from "../schema/model.js"
import bcrypt from "bcrypt"
import { sendEmail } from "../utils/sendmail.js"
import { secretKey } from "../constant.js"
import jwt from "jsonwebtoken"


export let createWebUser = async(req,res,next)=>{
    let data = req.body
    let hashPassword = await bcrypt.hash(data.password, 10)

    data = {
        ...data,password:hashPassword,isVerifiedEmail:false
    }
    try {
        let result = await WebUser.create(data)

        let infoObject = {
                _id : result._id,
            }
            
            let expiryInfo = {
                expiresIn : "5d",
             
            }
            
            let token = jwt.sign(infoObject, secretKey, expiryInfo)
        
        console.log(token)
        sendEmail({
            from: "Unique systems <burningonfire@gmail.com>",
            to: [data.email],
            subject: "Registration successfull.",
            html:`
            <div>
            <p> you have sucessfully registered in our system.</p>
            <p>Please click the link below to activate your account.</p>
            <p>
            <a href="http://localhost:8000/verify-email?token=${token}"> http://localhost:8000/verify-email?token=${token}</a>
            </p>
            `,
        })

        res.json({

            success: true,
            message: "user created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let verifyEmail = async (req,res,next) => {
    
    try {
    let tokenString =  req.headers.authorization
    let token = (tokenString.split(" ")[1])
    let infoObj = jwt.verify(token, secretKey)
    //console.log(infoObj)

    let userId = infoObj._id
    let result = await WebUser.findByIdAndUpdate(userId, 
        {isVerifiedEmail:true}, 
        {new: true})

        res.json({

            success: true,
            message: "email verified successfully.",
            result: result,
        })
        
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

export const loginUser = async (req,res,next) => {
try {
    let email = req.body.email
    let password = req.body.password
    let user = await WebUser.findOne({email:email})
    
    if (user){
        let isPasswordMatch = await bcrypt.compare(req.body.password, user.password)
        if(isPasswordMatch){
            let infoObject = {
                _id : user._id
            }

            let expiryInfo = {
                expiresIn : "365d",
            
            }

            let token = jwt.sign(infoObject, secretKey, expiryInfo)

        res.json({
            success: true,
            message: "user login successful",
            data: token,
        })
        }
        else {
            let error = new Error("incorrect password")
            throw error
            }
    }
    
} catch (error) {
    res.json({
        success: false,
        message: error.message,
    })

  
}
}

export const myProfile = async (req,res,next) => {
    try {
        let _id = req._id
        let result = await WebUser.findById (_id)
        res.json({
            success: true,
            message: "Profile successfully read",
            result: result,
        })
     } catch (error) {
       res.json({ 
        success: false,
        message: "Unable to read profile",
    })  
    }
}

export const updateMyProfile = async (req,res,next)=>{

 try {
    let _id = req._id
    let data = req.body
    delete data.email
    delete data.password

    let result = await WebUser.findByIdAndUpdate(_id, data,{new:true})

    res.json({
        success: true,
        message: "successfully updated profile",
        result: result,
    })
 } catch (error) {
    res.json({
        success: false,
        message: error.message,
    })
 }
}



//all of the function below are on /admin 
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

export let getAllWebUser = async (req,res,next)=>{
    try {
    // let result = await WebUser.find({})
    
    //pagination
    //localhost:8000/WebUsers?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    let result = await WebUser.find({}).skip((page-1)*limit).limit(limit)
        res.json({

            success: true,
            message: "All user read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all user successfully.",
        })
    }   
}

export let getSpecificWebUser = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await WebUser.findById(id)
        
        res.json({

            success: true,
            message: "user read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateWebUser = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await WebUser.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "user updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteWebUser = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await WebUser.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "user deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
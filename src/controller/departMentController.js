import { Department } from "../schema/model.js"

export let createDepartMent
 = async(req,res,next)=>{
    let data = req.body
    try {
        let result = await Department.create(data)
        res.json({

            success: true,
            message: "Department created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            message: error.message
        })
    }
}

export let getAllDepartMent = async (req,res,next)=>{
    try {
        let result = await Department.find({})

        res.json({

            success: true,
            message: "All department read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all department successfully.",
        })
    }
    
    

    
}

export let getSpecificDepartMent = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Department.findById(id)
        
        res.json({

            success: true,
            message: "Department read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateDepartMent = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Department.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "DepartMent updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteDepartMent = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Department.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "Department deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
import { Book } from "../schema/model.js"

export let createBook = async(req,res,next)=>{
    let data = req.body
    
    try {
        let result = await Book.create(data)

        res.json({

            success: true,
            message: "books created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create book.",
            message: error.message
        })
    }
}

export let getAllBook = async (req,res,next)=>{
    try {
        let result = await Book.find({})
        // let result = await Book.find({name: "nitan"})
        //let result = await Book.find({name:"nitan", roll: 50})

        //number searching
        //let result = await Book.find({roll:{$gt:50}})
        //let result = await Book.find({roll:{$gte:50}})
        //let result = await Book.find({roll:{$lt:50}})
        //let result = await Book.find({roll:{$lte:50}})
        //let result = await Book.find({roll:{$ne:70}})
        //let result = await Book.find({roll:{$in:[50,70,100]}})
       // let result = await Book.find({roll:{$gte:50,$lte:55}})

        
        //string searching
        //let result = await Book.find({name: {$in: ["nitan", "ram"]}})

        //regex searching
        // let result = await Book.find({name:"nitan"})
        // let result = await Book.find({name:/nitan/})
        // let result = await Book.find({name:/nitan/i})
        // let result = await Book.find({name:/ni/})
        // let result = await Book.find({name:/ni/i})
        // let result = await Book.find({name:/^ni/})
        // let result = await Book.find({name:/^ni/i})
        // let result = await Book.find({name:/ni$/})

        //select
        //select field only includes +element or -element not both at the same time except - _id
        //find has control over the object whereas select has control over the object properties
        // let result = await Book.find({}).select("name gender -_id")
        // let result = await Book.find({}).select("name")
        // let result = await Book.find({}).select("-name -gender -age") 

        //.find({}).sort("name") (ascending sort)
        //.find({}).sort("-name")(- will trigger descending sort)
        //.find({}).sort("name age")
        //.find({}).sort("-name age" )
        //.find({}).sort("age -name")
        //let result = await Book.find({}).select("name").sort("name")

        //skip
        //let result = await Book.find({})

        //limit
        //let result = await Book.limit({})
        
        //the order find works based upon below
        //find, sort, select, skip, limit

        res.json({
        
            success: true,
            message: "book read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read book successfully.",
        })
    }
    
    

    
}

export let getSpecificBook = async(req,res,next)=>{
    let id = req.params.id
    console.log(id)

    try {
        let result = await Book.findById(id)
        
        res.json({

            success: true,
            message: "book read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateBook = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Book.findByIdAndUpdate(id, data, {new: true}) 
    //if {new:false} will throw old data
    //if{new: true} will throw new data

    res.json({
        success : true,
        message: "book updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteBook = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Book.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "book deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
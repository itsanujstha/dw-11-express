import { Review } from "../schema/model.js"

export let createReview = async(req,res,next)=>{
    let data = req.body
    console.log("in post")
    try {
        let result = await Review.create(data)
        res.json({

            success: true,
            message: "review created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllReview = async (req,res,next)=>{
    try {
    // let result = await Review.find({})
    
    //pagination
    //localhost:8000/reviews?limit=2&page=2 on url 
    let limit = req.query.limit
    let page = req.query.page
    
    //let result = await Review.find({}).skip((page-1)*limit).limit(limit)
    let result = await Review.find({}).populate("productID").populate("userID")
    
        res.json({

            success: true,
            message: "All review read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read all review successfully.",
        })
    }
    
    

    
}

export let getSpecificReview = async(req,res,next)=>{
    let id = req.params.id
    //console.log(id)

    try {
        let result = await Review.findById(id)
        
        res.json({

            success: true,
            message: "review read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateReview = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Review.findByIdAndUpdate(id, data, {new: true})

    res.json({
        success : true,
        message: "review updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteReview = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Review.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "review deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
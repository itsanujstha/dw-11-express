import { Student } from "../schema/model.js"

export let createStudent = async(req,res,next)=>{
    let data = req.body
    
    try {
        let result = await Student.create(data)

        res.json({

            success: true,
            message: "students created successfully.",
            result: result,
        })


    } catch (error) {
        res.json({

            success: false,
            //message: "Unable to create student.",
            message: error.message
        })
    }
}

export let getAllStudent = async (req,res,next)=>{
    try {
        // let result = await Student.find({})
        // let result = await Student.find({name: "nitan"})
        //let result = await Student.find({name:"nitan", roll: 50})

        //number searching
        //let result = await Student.find({roll:{$gt:50}})
        //let result = await Student.find({roll:{$gte:50}})
        //let result = await Student.find({roll:{$lt:50}})
        //let result = await Student.find({roll:{$lte:50}})
        //let result = await Student.find({roll:{$ne:70}})
        //let result = await Student.find({roll:{$in:[50,70,100]}})
       // let result = await Student.find({roll:{$gte:50,$lte:55}})

        
        //string searching
        //let result = await Student.find({name: {$in: ["nitan", "ram"]}})

        //regex searching
        // let result = await Student.find({name:"nitan"})
        // let result = await Student.find({name:/nitan/})
        // let result = await Student.find({name:/nitan/i})
        // let result = await Student.find({name:/ni/})
        // let result = await Student.find({name:/ni/i})
        // let result = await Student.find({name:/^ni/})
        // let result = await Student.find({name:/^ni/i})
        // let result = await Student.find({name:/ni$/})

        //select
        //select field only includes +element or -element not both at the same time except - _id
        //find has control over the object whereas select has control over the object properties
        // let result = await Student.find({}).select("name gender -_id")
        // let result = await Student.find({}).select("name")
        // let result = await Student.find({}).select("-name -gender -age") 

        //.find({}).sort("name") (ascending sort)
        //.find({}).sort("-name")(- will trigger descending sort)
        //.find({}).sort("name age")
        //.find({}).sort("-name age" )
        //.find({}).sort("age -name")
        //let result = await Student.find({}).select("name").sort("name")

        //skip
        //let result = await Student.find({})

        //limit
        //let result = await Student.limit({})
        
        //the order find works based upon below
        //find, sort, select, skip, limit

        res.json({
        
            success: true,
            message: "students read successfully.",
            result: result,
        })

    } catch (error) {
        res.json({

            success: false,
            message: "Unable to read students successfully.",
        })
    }
    
    

    
}

export let getSpecificStudent = async(req,res,next)=>{
    let id = req.params.id
    console.log(id)

    try {
        let result = await Student.findById(id)
        
        res.json({

            success: true,
            message: "students read successfully.",
            result: result,
        })
    } catch (error) {
        res.json({

            success: false,
            message: error.message,
        })
    }
    
    

    
    
}

export let updateStudent = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

try {
    let result = await Student.findByIdAndUpdate(id, data, {new: true}) 
    //if {new:false} will throw old data
    //if{new: true} will throw new data

    res.json({
        success : true,
        message: "students updated successfully.",
        result: result,
    })
    
} catch (error) {
    res.json({
        success : false,
        message: error.message,
    })
    
}  
}

export let deleteStudent = async(req,res,next)=>{
    let id = req.params.id

    try {
    let result = await Student.findByIdAndDelete(id)

    //to produce error in case of specific id doesn't exist
    //{} if data
    //null id if no data

    if(result === null ){
        res.json({
            success: false,
            message: "id does not exist"
        })
    }
    else {
        res.json({
            success : true,
            message: "students deleted successfully.",
            result : result,
        })
    }

    
        
    } catch (error) {
        res.json({
            success : false,
            message:  error.message,
        })
    }
    
}
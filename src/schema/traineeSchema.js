import { Schema } from "mongoose";

export let traineeSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    class: {
        type: String,
        required: true,
    },
    faculty: {
        type: String,
        required: true,
    },
})
import { Schema } from "mongoose";

export let productSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,

    },
    price:{
        type: Number,
        required: true,

    },
})
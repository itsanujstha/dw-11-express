import { Schema } from "mongoose";

export let studentSchema = Schema({
    name: {
        type: String,
        required: [true, "name field is required"],
        lowercase: true,
        trim: true,
        minLength: [3, "name must be at leaset 3 characters long"],
        maxLength: [15, "name must be max 15 characters long"],

        validate: (value) => {
            let onlyAlphabet = /^[A-Za-z]+$/.test(value)

            if( onlyAlphabet){

            }
            else {
                let error = new Error("name field must have only alphabet only")
                throw error;
            }
        }
        

    },
    phoneNumber:{
        type: Number,
        required: [true, "phoneNumber field is required"],
        validate: (value) => {
            let strPhoneNumber = String(value)
            if( strPhoneNumber.length === 10){

            }
            else {
                let error = new Error("phone number must be 10 character long")
                throw error;
            }
        }
    },

    gender : {
        type: String,
        default: "male",
        required: [true, "gender field is required"],
        validate: (value) => {
            if(value ==="male" || value === "female" || value === "other"){

            }
            else {
                let error = new Error("Gender must be etiher Male, Female or Other")
                throw error;
            }
        }
    },
    password: {
        type: String,
        required: [true, "password is required"],

        validate:(value)=>{
            
            let isValidPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/.test(value)
            
            if(!isValidPassword){
                throw new Error ("email must be valid")
            }
        }
        
        //password must have 1 number, one lower case, one uppercase, one symbol, min 8 char, max 15 char

    },
    
    roll: {
        type: Number,
        required: [true, "roll number is required"],
        min: [50, "roll no. between 50 to 100 are elligible"],
        max: [100, "roll no. between 50 to 100 are elligible"],


    },

    isMarried: {
        type: Boolean,
        required: [true, "isMarried field is required"]

    },
    spouseName: {
        type: String,
        required: [true, "spouseName is required"]

    },

    email: {
        type: String,
        required: [true, "email is required"],
        unique: true,
        validate:(value)=>{
            let isValidEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)

            // if (isValidEmail) 
            // {

            // }
            // else{
            //     throw new Error("email must be vlaid")
            // }
            
            if(!isValidEmail){
                throw new Error ("email must be valid")
            }
        }
    },
    
    dob: {
        type: Date,
        required: [true, "dob field is required"]

    },
    
    location: {
        country: {
            type: String,
            required: [true, "country is required"]
        
        },
        exactLocation:{
            type: String,
            required : [true, "exact location is required"]
        }

    },

    favTeacher: [{
        type: String,
    }],

    favSubject : [
        {
            bookName:{
                type: String,
                required: [true, "bookname is required"]
            },
            bookAuthor: {
                type: String,
                required: [true, "bookAuthor is required"]
            }
        }],

  

})




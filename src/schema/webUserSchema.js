import { Schema } from "mongoose";

export let webUserSchema = Schema({
    fullName: {
        type: String,
        required: [true, "fullName is required"]
    },
    email:{
        type: String,
        required: [true, "Email is required"],
        unique: true

    },
    password:{
        type: String,
        required: [true, "password is required"]

    },
    dob:{
        type: Date,
        required: [true, "Date of Birth is required"]

    },
    gender:{
        type: String,
        required: [true, "gender is required"]

    },
    role:{
        type: String,
        required: [true, "role is required"]

    },
    isVerifiedEmail:{
        type: Boolean,
        },
}, {timestamps:true})
import { config } from "dotenv"
config()

export let port = process.env.PORT
export let mongoUrl = process.env.MONGO_URL
export let secretKey = process.env.SECRET_KEY

export let email = process.env.EMAIL
export let pass = process.env.PASS